using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ButtonInteraction : MonoBehaviour
{
    public void OnButtonPressed()
    {
        Debug.Log("Button pressed.");
    }
}
