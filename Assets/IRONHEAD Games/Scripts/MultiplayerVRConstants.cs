

public class MultiplayerVRConstants
{
    public const string MAP_TYPE_KEY = "map";
    public const string MAP_TYPE_OUTDOOR = "outdoor";
    public const string MAP_TYPE_SCHOOL = "school";

    public const string AVATAR_SELECTION_NUMBER = "Avatar_Selection_Number";
}
